# Проект представляет весеннюю школу Java компании EPAM, 2016 год. #


### Настройка среды разработки: ###

1. [Установка и настройка Git](https://bitbucket.org/elefus/spring-java-school-epam-2016/wiki/SetupGit).

2. [Загрузка проекта](https://bitbucket.org/elefus/spring-java-school-epam-2016/wiki/DownloadProject).

3. [Настройка IntellijIdea](https://bitbucket.org/elefus/spring-java-school-epam-2016/wiki/SetupIdea).

4. [Просмотр результатов](https://bitbucket.org/elefus/spring-java-school-epam-2016/wiki/Results).

-------------------------------------------------------------------------------------------------------------

### Описание практических заданий: ###

1. [Первое задание](https://bitbucket.org/elefus/spring-java-school-epam-2016/wiki/%D0%97%D0%B0%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5%20%E2%84%961).

2. [Второе задание](https://bitbucket.org/elefus/spring-java-school-epam-2016/wiki/%D0%97%D0%B0%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5%20%E2%84%962).

3. [Третье задание](https://bitbucket.org/elefus/spring-java-school-epam-2016/wiki/%D0%97%D0%B0%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5%20%E2%84%963).